如何管理第三方软件风险
一个项目常常依赖于组织生产的软件，却不控制软件。参与进来的每一个人必须意识到第三方软件有很大的风险。
从不，绝不要对vapour报任何希望。vapour是已经被承诺但却未提供的软件。这是最可靠的方式导致破产。仅仅怀疑一个软件公司承诺在一个确定的时间发布一个具有确定的特征的确定的产品是不理智的。完全地忽视它或者忘记你曾听到关于它的是更理智的。从来不要写下你公司使用的文件。
如果第三方软件不是vapour，它仍然具有风险，但是它至少是可以处理的风险。如果你正在使用第三方软件，你应该尽早投入精力去计算它。人们可能不太喜欢听到需要花费两周或者两个月去评价三个产品中任何一个的适用性，但是它需要尽可能早去做。没有适当的评价，集成的成本不能准确地估计。
为一个特定的目的，理解现存的第三方软件的适用性是部落的知识。这是主观的并且通常专家所具有。如果你能找到这些专家，你可以节省很多时间。通常一个项目完全地依赖于第三方软件系统，因此集成失败了，项目也将会失败。在日程安排中明确地表达风险。如果风险不能被早点移除，尝试制定一个应急的计划，比如可以使用并且具有自我实现功能的系统。绝不要把时间表依赖于vapour。